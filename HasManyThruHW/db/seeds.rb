# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Patient.create(name:'Irving Johns', street_address: '1234 Wocky Rd.')
Patient.create(name:'Mike Johns', street_address: '1234 Socky Rd.')
Patient.create(name:'Keith Johns', street_address: '1234 Kocky Rd.')
Patient.create(name:'Jimi Johns', street_address: '1234 Yooky Rd.')
Patient.create(name:'Jordan Johns', street_address: '1234 :Looky Rd.')

Insurance.create(name:'Yellow Cross', street_address: '1234 Rocky Rd.')
Insurance.create(name:'United Hell', street_address: '1234 Grassy Rd.')
Insurance.create(name:'Geico', street_address: '1234 Glossy Rd.')
Insurance.create(name:'Discount', street_address: '1234 Foggy Rd.')
Insurance.create(name:'Johnson Insurance', street_address: '1234 Knocky Rd.')