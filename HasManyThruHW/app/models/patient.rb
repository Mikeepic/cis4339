class Patient < ActiveRecord::Base
  attr_accessible :name, :street_address
  has_many :appointments
  has_many :specialists, :through => :appointments
  has_many :insurances

  INSURANCES = ['Yellow Cross, Geico, Discount, United Hell, Johnson Insurance']
end
