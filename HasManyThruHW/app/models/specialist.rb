class Specialist < ActiveRecord::Base
  attr_accessible :name, :specialty
  has_many :appointments
  has_many :patients, :through => :appointments
end
