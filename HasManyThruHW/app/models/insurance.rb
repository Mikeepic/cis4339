class Insurance < ActiveRecord::Base
  attr_accessible :name, :street_address
  has_many :patients
end
