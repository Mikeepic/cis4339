class Appointment < ActiveRecord::Base
  attr_accessible :appointment_date, :complaint, :patient_id, :specialist_id
  belongs_to :specialist
  belongs_to :patient
end
